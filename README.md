## edX MIT 6.00x PSETS

_Fall 2012, Spring 2013_

_Language: Python_

PSET2: Bisection.
PSET3: Hangman, Successive Approximation, Newton-Raphson.
PSET4: Wordgame.
PSET5: Substitution Cipher(Shifting), Brute force decryption.
PSET6: Classes and Objects, News Feed (In-complete).
PSET8: Monte Carlo Simulations, Simulating Virus Population.