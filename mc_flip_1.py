import random
def flip(numFlips):
    heads = 0
    for i in range(numFlips):
        if random.random() < 0.5:
            heads += 1
    return heads / float(numFlips)

for i in range(5):
    print flip(1000000)
