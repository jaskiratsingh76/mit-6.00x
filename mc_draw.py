import random

bag = ['R', 'R', 'R', 'G', 'G', 'G']
drawnBalls = []

def draw():
    bag = ['R', 'R', 'R', 'G', 'G', 'G']
    drawnBalls = []
    for i in range(3):
        ball = random.choice(bag)
        bag.remove(ball)
        drawnBalls.append(ball)

    #check if all balls are same
    if drawnBalls[0] == drawnBalls[1] and drawnBalls[1] == drawnBalls[2]:
        return True
    return False

def noReplacementSimulation(numTrials):
    '''
    Runs numTrials trials of a Monte Carlo simulation
    of drawing 3 balls out of a bucket containing
    3 red and 3 green balls. Balls are not replaced once
    drawn. Returns the a decimal - the fraction of times 3 
    balls of the same color were drawn.
    '''
    allGreen = 0
    for i in range(numTrials):
        if draw():
            allGreen += 1
    return float(allGreen)/float(numTrials)

print noReplacementSimulation(1000000)
