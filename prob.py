def probTest(limit):
    rolls = 0
    prob = 1.0/6.0
    while prob >= limit:
        #roll repeatedly
        rolls += 1
        prob = prob * (5.0/6.0)
    return rolls + 1
