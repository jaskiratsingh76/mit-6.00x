import random, pylab

def sampleQuizzes():
    scores = []
    for trial in xrange(0, numTrials):
        midterm1 = random.randrange(50, 81)
        midterm2 = random.randrange(60, 90)
        final = random.randrange(55, 96)

        final_score = (0.25 * (midterm1 + midterm2)) + (0.5 * final)
        scores.append(final_score)
    return scores

def plotQuizzes():
    # Your code here
    pass
    scores = sampleQuizzes()
    pylab.title('Distribution of Scores')
    pylab.xlabel('Final Score')
    pylab.ylabel('Number of Trials')
    pylab.hist(scores, bins = 15)
    pylab.show()

numTrials = 100000
plotQuizzes()
